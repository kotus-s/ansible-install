# ✨ Quick Ansible installation

Quick Ansible installation is a bunch of playbook to quickly install and secure your server.

[[_TOC_]]

## 🔨 Prerequisite

In order, you must install ansible and git on your server. Asumming you've got a Centos 7 server, you should do the following:

```shell
yum install -y epel-release ansible python-argcomplete libselinux-python git tree nano
activate-global-python-argcomplete
```

Then you should clone the repo

```shell
git clone https://gitlab.com/kotus-s/ansible-install.git ansible-install
```

Before all, you should copy the `env.example` as `env` and edit the content according to your settings

```shell
cd ansible-install && cp env.example env
```

## ✌️ Usage

Now, just run the play book with the following command

```shell
ansible-playbook centos7.yml
```

## 🌟 Features

- Upgrading all packages
- Allow sudoers to not use password on sudo command
- Adding users relative to configuration file
- Disabling root login
- Changing SSH port relative to configuration file
- Enable and configure firewalld

## ⚙️ Configuration file (env)

>>>
⚠️ Make sur to keep env.example, just make a copy of it and rename it to env
>>>

- `allow_sudoers_without_password` (default:**true**|false) *Should allow passwordless sudoers ?*
- `users` *List of users that should be created on server*
  - In `users` list, each user should follow the following structure:

    ```yml
      users:
        - name: "kotus" # Login of the user
          password: "MySuperSecretPassword_" # Password of the user
          groups: "wheel" # Groups associated to the user, you can type any groups as you want by separted each with comma (,)
          generate_ssh_key: true # Generate SSH key for this user ?
      ```

- `ssh_port` (default:**224**) *New SSH port*

## ✍️ TODO

- [ ] Removing users where not in configuration file
- [ ] Add method to open/close any port you want in firewalld
- [ ] Make it generic for all operating system

## 📄 License

[MIT](https://choosealicense.com/licenses/mit/)
